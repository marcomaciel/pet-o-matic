package petomatic.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtils {

        
        static String line = null;
        /**
         * Salva o conteudo de uma variavel em um arquivo
         * 
         * @param file
         * @param content
         * @param adicionar
         *            se true adicionar no final do arquivo
         * @throws IOException
         */
        public static void save(String file, String content, boolean adicionar)
                        throws IOException {

                FileWriter fw = new FileWriter(file, adicionar);
                content += "\n";
                fw.write(content);
                fw.close();
        }

        /**
         * Carrega o conteudo de um arquivo em uma String, se o aquivo nao existir,
         * retornara null.
         * 
         * @param fileName
         * @return conteudo
         * @throws Exception
         */
        public static String loadFile(String fileName)
                        throws FileNotFoundException, IOException {

                File file = new File(fileName);

                if (!file.exists()) {
                        return null;
                }

                BufferedReader br = new BufferedReader(new FileReader(fileName));
                StringBuffer bufOut = new StringBuffer();

                String line;
                while ((line = br.readLine()) != null) {
                        bufOut.append(line + "\n");
                }
                br.close();
                return bufOut.toString();
        }

        public static String getLastLine(String fileName)
                        throws FileNotFoundException, IOException {

                File file = new File(fileName);
                if (!file.exists()) {
                        return null;
                }

                BufferedReader br = new BufferedReader(new FileReader(fileName));
                line = null;
                while (br.ready()) {
                        line = br.readLine();
                }
                br.close();
                return line;
        }
}