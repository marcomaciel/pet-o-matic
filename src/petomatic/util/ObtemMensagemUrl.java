package petomatic.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class ObtemMensagemUrl {

	public static String lerUrl() {
		StringBuilder resultado = new StringBuilder();
		try {
			URL url = new URL("http://turn-on.appspot.com/obtem_mensagem");
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String line;

			while ((line = reader.readLine()) != null) {
				resultado.append(line);
			}
			reader.close();

		} catch (Exception e) {
		}
		return resultado.toString();

	}

}
