package petomatic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import petomatic.comm.SerialComm;
import petomatic.util.FileUtils;
import petomatic.util.ObtemMensagemUrl;

public class BatchCommands extends Thread {
	
	String logFile = "tweets.log";
	String lastLine = null;
	long lastDateTimeFromLog = 0l;
	TwitterController twController = new TwitterController();
	
	public void run(){
		Properties prop = new Properties();
		String modo = "TWITTER";
		Integer time = 5000;
		
		try {
			prop.load(new FileInputStream("/etc/pet/config.properties"));
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		modo = prop.getProperty("modoPost");
		time = new Integer(prop.getProperty("time"));
		
		
		while (true){		
			
			if (modo.equals("TWITTER")){
				if (readFromTwitter()){
					sendFeedMsg();
				}
			}else{
				if (readFromUrl()){
					sendFeedMsg();
				}
			}
			
			
			//
			
			try{
				//Thread.sleep(1000 * 60 * 15);
				Thread.sleep(time);
			}catch(Exception ex){
				System.out.println("Erro no run: " + ex.getMessage());
			}
		}
	}

	private void sendFeedMsg() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy HH:mm");
		String dateTimeStr = sdf.format(new Date());
		twController.sendTweet("Alimentado em " + dateTimeStr);
	}

	private boolean readFromUrl() {
		String tweet = ObtemMensagemUrl.lerUrl();
		
		if (tweet.toLowerCase().contains("1")){
			//new SerialComm().sendCommand("A");
			System.out.println("comando enviado");
			return true;
		}else{
			System.out.println("bahhhh");
			return false;
		}
	}

	private boolean readFromTwitter() {
		String tweet = twController.lerUltimoTweet();
		try {
			lastDateTimeFromLog = getLastDateTimeFromLog(logFile);
			
			long dateTimeFromTweet = new Long(tweet.split(";")[0]);
			String msgFromTweet = tweet.split(";")[1];
			
			
			if (dateTimeFromTweet > lastDateTimeFromLog){
				if (msgFromTweet.toLowerCase().contains("#alimentar")){
					new SerialComm().sendCommand("A");
					FileUtils.save(logFile, tweet, true);
					return true;
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public long getLastDateTimeFromLog(String fileName) throws FileNotFoundException, IOException {
	       lastLine = FileUtils.getLastLine(fileName);
	       long lastDateTime = 0;
	       if (lastLine != null) {
	           lastDateTime = Long.parseLong(lastLine.substring(0, lastLine.indexOf(";")));
	       } else {
	           lastDateTime = 0L;
	       }
	       return lastDateTime;
	   }
}
