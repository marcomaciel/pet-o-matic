package petomatic.comm;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.things.SerialController;

public class SerialComm {
	
	public void sendCommand(String string) {
		SerialController sc;
		Properties prop = new Properties();
		String port = "/dev/tty.usbserial-A900IQ7X";
		try {
			prop.load(new FileInputStream("/etc/pet/config.properties"));
			port = prop.getProperty("serialport");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		System.out.println("Port: " + port);
		try {
			
			sc = new SerialController(port);
			boolean status = sc.sendCommand(string);
			System.out.println("Status do comando: " + status);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
